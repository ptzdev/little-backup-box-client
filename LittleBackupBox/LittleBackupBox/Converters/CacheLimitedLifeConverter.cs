﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace LittleBackupBox.Converters
{
    public class CacheLimitedLifeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is string))
            {
                throw new InvalidOperationException("Target must be a url string");
            }

            return new UriImageSource
            {
                Uri = new Uri((string)value),
                CachingEnabled = true,
                CacheValidity = new TimeSpan(5, 0, 0)
            };
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
