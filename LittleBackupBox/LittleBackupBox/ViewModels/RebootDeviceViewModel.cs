﻿using LittleBackupBox.Interfaces;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LittleBackupBox.ViewModels
{
    public class RebootDeviceViewModel : BindableBase
    {
        #region Properties
        DelegateCommand _buttonNo;
        DelegateCommand _buttonYes;

        public DelegateCommand ButtonYes => _buttonYes != null ? _buttonYes : (_buttonYes = new DelegateCommand(ButtonYesClicked));
        public DelegateCommand ButtonNo => _buttonNo != null ? _buttonNo : (_buttonNo = new DelegateCommand(ButtonNoClicked));

        private IContentService _contentService;
        private INavigationService _navigationService;
        private ILBBGatewayService _gatewayService;
        private IPageDialogService _dialogService;

        private string _buttonNoText;
        private string _buttonYesText;
        private string _confirmActionText;

        private string content_Language = "PT";

        private string contentKey_ConfirmActionText = "RebootDeviceViewModel_ConfirmActionText";
        private string contentKey_ButtonYes = "RebootDeviceViewModel_ButtonYes";
        private string contentKey_ButtonNo = "RebootDeviceViewModel_ButtonNo";
        private string contentKey_sucessMsg = "RebootDeviceViewModel_SucessMsg";
        private string contentKey_sucessOk = "RebootDeviceViewModel_SucessOk";
        private string contentKey_errorTitle = "RebootDeviceViewModel_ErrorTitle";
        private string contentKey_sucessTitle = "RebootDeviceViewModel_SucessTitle";
        private string contentKey_errorOk = "RebootDeviceViewModel_ErrorOk";

        public string ButtonNoText
        {
            get { return _buttonNoText; }
            set { SetProperty(ref _buttonNoText, value); }
        }

        public string ButtonYesText
        {
            get { return _buttonYesText; }
            set { SetProperty(ref _buttonYesText, value); }
        }

        public string ConfirmActionText
        {
            get { return _confirmActionText; }
            set { SetProperty(ref _confirmActionText, value); }
        }
        #endregion

        #region Constructor
        public RebootDeviceViewModel(
            INavigationService navigationService,
            IContentService contentService,
            ILBBGatewayService gatewayService,
            IPageDialogService dialogService)
        {
            _contentService = contentService;
            _navigationService = navigationService;
            _gatewayService = gatewayService;
            _dialogService = dialogService;

            this.SetText();
        }
        #endregion

        private void SetText()
        {
            ConfirmActionText = _contentService.GetContent(contentKey_ConfirmActionText, content_Language);
            ButtonYesText = _contentService.GetContent(contentKey_ButtonYes, content_Language);
            ButtonNoText = _contentService.GetContent(contentKey_ButtonNo, content_Language);
        }

        private void ButtonNoClicked()
        {

        }

        private async void ButtonYesClicked()
        {
            string errors = "";
            string sucessMsg = _contentService.GetContent(contentKey_sucessMsg, content_Language);
            string sucessTitle = _contentService.GetContent(contentKey_sucessTitle, content_Language);
            string sucessOk = _contentService.GetContent(contentKey_sucessOk, content_Language);
            string errorTitle = _contentService.GetContent(contentKey_errorTitle, content_Language);
            string errorOk = _contentService.GetContent(contentKey_errorOk, content_Language);

            var response = await _gatewayService.RebootDevice();
            if (response.WasSuccessful)
            {
                await _dialogService.DisplayAlertAsync(sucessTitle, sucessMsg, sucessOk);
            }
            else
            {
                await _dialogService.DisplayAlertAsync(errorTitle, errors, errorOk);
            }
        }
    }
}
