﻿using LittleBackupBox.Interfaces;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.AndroidSpecific;

namespace LittleBackupBox.ViewModels
{
    public class LandingScreenViewModel : BindableBase, INavigationAware
    {
        DelegateCommand _getStartedCommand;

        private string wifiSSID = "\"LBB-Hotspot\"";

        #region Content Keys
        private string contentKey_GetStarted = "LandingScreenViewModel_GetStarted";
        private string contentKey_TitleApp = "LandingScreenViewModel_TitleApp";
        private string contentKey_Description = "LandingScreenViewModel_Description";
        private string contentKey_WarningText = "LandingScreenViewModel_WarningText";
        private string contentKey_ErrorText = "LandingScreenViewModel_ErrorText";
        private string content_Language = "PT"; 
        #endregion

        #region Constructor
        public LandingScreenViewModel(INavigationService navigationService, ILBBGatewayService gatewayService, IContentService contentService)
        {
            _contentService = contentService;
            _navigationService = navigationService;
            _gatewayService = gatewayService;

            this.SetText();
        }
        #endregion

        #region Services
        private IContentService _contentService;
        private INavigationService _navigationService;
        private ILBBGatewayService _gatewayService;
        #endregion

        #region Text Properties
        private string _buttonGetStartedText;
        public string ButtonGetStartedText
        {
            get { return _buttonGetStartedText; }
            set { SetProperty(ref _buttonGetStartedText, value); }
        }

        private string _titleAppText;
        public string TitleAppText
        {
            get { return _titleAppText; }
            set { SetProperty(ref _titleAppText, value); }
        }

        private string _descriptionText;
        public string DescriptionText
        {
            get { return _descriptionText; }
            set { SetProperty(ref _descriptionText, value); }
        }

        private string _warningTextt;
        public string WarningText
        {
            get { return _warningTextt; }
            set { SetProperty(ref _warningTextt, value); }
        }

        private void SetText()
        {
            ButtonGetStartedText = _contentService.GetContent(contentKey_GetStarted, content_Language);
            TitleAppText = _contentService.GetContent(contentKey_TitleApp, content_Language);
            DescriptionText = _contentService.GetContent(contentKey_Description, content_Language);
            WarningText = _contentService.GetContent(contentKey_WarningText, content_Language);
        }
        #endregion

        #region Button

        public DelegateCommand GetStartedCommand => _getStartedCommand != null ? _getStartedCommand : (_getStartedCommand = new DelegateCommand(GetStartedClicked));

        private async void GetStartedClicked()
        {
            if (DependencyService.Get<IWifiManagerService>().IsExpectedSSID(wifiSSID))
            {
                _gatewayService.InitializeService("http://192.168.42.1", 3000, "lbbGateway", "lbbGateway");

                var p = new NavigationParameters();

                await _navigationService.NavigateAsync("MainPage/ListPhotos", p);
            }
            else
            {
                WarningText = _contentService.GetContent(contentKey_ErrorText, content_Language);
            }
        } 
        #endregion

        #region INavigationAware
        public void OnNavigatedFrom(NavigationParameters parameters)
        {

        }

        public void OnNavigatingTo(NavigationParameters parameters)
        {

        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {

        }
        #endregion
    }
}
