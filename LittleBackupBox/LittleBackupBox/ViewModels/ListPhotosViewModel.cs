﻿using LittleBackupBox.Interfaces;
using LittleBackupBox.Model;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Linq;
using Xamarin.Forms;

namespace LittleBackupBox.ViewModels
{
    public class ListPhotosViewModel : BindableBase, INavigationAware
    {
        private readonly INavigationService _navigationService;
        private bool _isRefreshing = false;
        private bool _haveFiles = false;
        private bool _noFiles = false;
        private ObservableCollection<File> _files;
        private ILBBGatewayService _gatewayService;
        private DelegateCommand<File> _fileSelectedCommand;

        public DelegateCommand<File> FileSelectedCommand => _fileSelectedCommand != null ? _fileSelectedCommand : (_fileSelectedCommand = new DelegateCommand<File>(FileSelected));

        public ObservableCollection<File> Files
        {
            get { return _files; }
            set
            {
                this.HaveFiles = value.Any();
                this.NoFiles = !value.Any();
                SetProperty(ref _files, value);
            }
        }

        public bool IsRefreshing
        {
            get { return _isRefreshing; }
            set
            {
                SetProperty(ref _isRefreshing, value);
            }
        }

        public bool HaveFiles
        {
            get { return _haveFiles; }
            set
            {
                SetProperty(ref _haveFiles, value);
            }
        }

        public bool NoFiles
        {
            get { return _noFiles; }
            set
            {
                SetProperty(ref _noFiles, value);
            }
        }

        public ListPhotosViewModel(ILBBGatewayService gatewayService, INavigationService navigationService)
        {
            this._gatewayService = gatewayService;
            this._navigationService = navigationService;
        }

        public async void OnNavigatedFrom(NavigationParameters parameters)
        {
            IsRefreshing = true;

            if (Files == null)
                Files = new ObservableCollection<File>(await _gatewayService.ListFiles());

            IsRefreshing = false;
        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {

        }

        public async void OnNavigatingTo(NavigationParameters parameters)
        {
            IsRefreshing = true;

            if (Files == null)
                Files = new ObservableCollection<File>(await _gatewayService.ListFiles());

            IsRefreshing = false;
        }

        public ICommand RefreshCommand
        {
            get
            {
                return new Command(async () =>
                {
                    IsRefreshing = true;

                    Files = new ObservableCollection<File>(await _gatewayService.ListFiles());

                    IsRefreshing = false;
                });
            }
        }

        private async void FileSelected(File file)
        {
            var p = new NavigationParameters();
            p.Add("file", file);

            await _navigationService.NavigateAsync("NavigationPage/PhotoDetail", p);
        }
    }
}
