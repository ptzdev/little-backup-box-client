﻿using LittleBackupBox.Model;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LittleBackupBox.ViewModels
{
    public class PhotoDetailViewModel : BindableBase, INavigationAware 
    {
        private File _File;
        public File File
        {
            get { return _File; }
            set
            {
                SetProperty(ref _File, value);
            }
        }

        public PhotoDetailViewModel()
        {

        }

        public void OnNavigatingTo(NavigationParameters parameters)
        {
            if (parameters.ContainsKey("file"))
                File = (File)parameters["file"];
        }

        public void OnNavigatedFrom(NavigationParameters parameters)
        {
        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {
        }
    }
}
