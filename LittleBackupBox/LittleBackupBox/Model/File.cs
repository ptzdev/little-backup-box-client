﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LittleBackupBox.Model
{
    public class File
    {
        public string Filename { get; set; }
        public DateTime Date { get; set; }
        public string Image { get; set; }

        [JsonIgnore]
        public string Img
        {
            get
            {
                return string.IsNullOrEmpty(Image) ? "noPhoto.png" : Image;
                //return "noPhoto.png";
            }
        }

        [JsonIgnore]
        public string FullImg
        {
            get
            {
                return Img.Replace("_thumb.JPG", ".JPG");
                //return "noPhoto.png";
            }
        }

        [JsonIgnore]
        public string Title
        {
            get
            {
                var pos = Filename.LastIndexOf("/") + 1;
                return Filename.Substring(pos, Filename.Length - pos);
            }
        }

        [JsonIgnore]
        public string SubTitle
        {
            get
            {
                return Date.ToString("yyyy-MM-dd hh:mm");
            }
        }
    }
}
