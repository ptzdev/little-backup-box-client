﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LittleBackupBox.Model
{
    public class RemoteResponse
    {
        private bool _wasSuccessful;
        private bool _error;
        private string _message;

        public bool WasSuccessful
        {
            get { return _wasSuccessful; }
            set { _wasSuccessful = value; }
        }

        public bool HaveErrors
        {
            get { return _error; }
            set { _error = value; }
        }

        public string ReturnMessage
        {
            get { return _message; }
            set { _message = value; }
        }

        public RemoteResponse(string message) : this(message, false) { }

        public RemoteResponse(string message, bool haveError)
        {
            this._error = haveError;
            this._message = message;
            this._wasSuccessful = !_error;
        }
    }
}
