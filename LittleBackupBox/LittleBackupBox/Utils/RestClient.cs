﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace LittleBackupBox.Utils
{
    public class RestClient :  IDisposable
    {
        private Uri url;
        private string authData;
        private HttpClient client;

        public RestClient(Uri url, string username, string password)
        {
            this.url = url;
            this.authData = string.Format("{0}:{1}", username, password);

            var authHeaderValue = Convert.ToBase64String(Encoding.UTF8.GetBytes(authData));

            this.client = new HttpClient();
            this.client.MaxResponseContentBufferSize = 256000;
            this.client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);
        }

        public void Dispose()
        {
            this.client.Dispose();
        }

    }
}
