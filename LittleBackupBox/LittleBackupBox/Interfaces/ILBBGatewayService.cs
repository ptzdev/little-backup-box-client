﻿using LittleBackupBox.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LittleBackupBox.Interfaces
{
    public interface ILBBGatewayService
    {
        void InitializeService(string address, int port, string username, string password);
        Task<RemoteResponse> EmptyDrive();
        Task<RemoteResponse> TurnOffDevice();
        Task<RemoteResponse> RebootDevice();
        Task<List<File>> ListFiles();
    }
}
