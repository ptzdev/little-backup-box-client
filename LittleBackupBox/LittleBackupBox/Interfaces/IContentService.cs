﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LittleBackupBox.Interfaces
{
    public interface IContentService
    {
        string GetContent(string contentKey, string language);
    }
}
