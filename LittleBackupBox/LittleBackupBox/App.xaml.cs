﻿using Prism.Ninject;
using LittleBackupBox.Views;
using Xamarin.Forms;
using LittleBackupBox.Interfaces;
using LittleBackupBox.Service;

namespace LittleBackupBox
{
    public partial class App : PrismApplication
    {
        public App(IPlatformInitializer initializer = null) : base(initializer) { }

        protected override void OnInitialized()
        {
            InitializeComponent();

            NavigationService.NavigateAsync("NavigationPage/LandingScreen");
        }

        protected override void RegisterTypes()
        {
            Container.Bind<IContentService>().To<ContentService>();
            Container.Bind<ILBBGatewayService>().To<LBBGatewayService>().InSingletonScope();

            Container.RegisterTypeForNavigation<NavigationPage>();
            Container.RegisterTypeForNavigation<LandingScreen>();
            Container.RegisterTypeForNavigation<MainPage>();
            Container.RegisterTypeForNavigation<ListPhotos>();
            Container.RegisterTypeForNavigation<EraseDrive>();
            Container.RegisterTypeForNavigation<RebootDevice>();
            Container.RegisterTypeForNavigation<TurnOffDevice>();
            Container.RegisterTypeForNavigation<PhotoDetail>();
        }
    }
}
