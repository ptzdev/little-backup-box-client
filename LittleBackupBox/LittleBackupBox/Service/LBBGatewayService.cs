﻿using LittleBackupBox.Interfaces;
using LittleBackupBox.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace LittleBackupBox.Service
{
    public class LBBGatewayService : ILBBGatewayService
    {
        private HttpClient client;
        private string listFilesUrl;
        private string emptyDrivesUrl;
        private string turnoffUrl;
        private string rebootUrl;
        private string url;

        public void InitializeService(string address, int port, string username, string password)
        {
            //On emulator url should be http://169.254.80.80:3000
            this.url = string.Format("{0}:{1}", address, port);

            this.listFilesUrl = string.Format("{0}/api/list", url);
            this.emptyDrivesUrl = string.Format("{0}/api/emptyDrive", url);
            this.turnoffUrl = string.Format("{0}/api/turnoff", url);
            this.rebootUrl = string.Format("{0}/api/reboot", url);

            var authData = string.Format("{0}:{1}", username, password);
            var authHeaderValue = Convert.ToBase64String(Encoding.UTF8.GetBytes(authData));

            client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);

        }

        public async Task<RemoteResponse> EmptyDrive()
        {
            try
            {
                var response = await client.PostAsync(new Uri(emptyDrivesUrl), null);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();

                    return new RemoteResponse(content);
                }
            }
            catch (Exception ex)
            {
                return new RemoteResponse(ex.Message, true);
            }

            return new RemoteResponse("Generic error on EmptyDrive", true);
        }

        public async Task<List<File>> ListFiles()
        {
            var files = new List<File>();
            var uri = new Uri(this.listFilesUrl);

            try
            {
                var response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();

                    files = GetFilesFromContent(content);
                }

                return files;
            }
            catch (Exception ex)
            {
                return files;
            }
        }

        public async Task<RemoteResponse> RebootDevice()
        {
            try
            {
                var response = await client.PostAsync(new Uri(rebootUrl), null);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();

                    return new RemoteResponse(content);
                }
            }
            catch (Exception ex)
            {
                return new RemoteResponse(ex.Message, true);
            }

            return new RemoteResponse("Generic error on RebootDevice", true);
        }

        public async Task<RemoteResponse> TurnOffDevice()
        {
            try
            {
                var response = await client.PostAsync(new Uri(turnoffUrl), null);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();

                    return new RemoteResponse(content);
                }
            }
            catch (Exception ex)
            {
                return new RemoteResponse(ex.Message, true);
            }

            return new RemoteResponse("Generic error on TurnOffDevice", true);
        }

        private List<File> GetFilesFromContent(string content)
        {
            var files = JsonConvert.DeserializeObject<List<File>>(content);

            foreach (var file in files)
            {
                if (file.Image.Contains("{url}"))
                {
                    file.Image = file.Image.Replace("{url}", this.url);
                }
            }
            return files;
        }
    }
}
