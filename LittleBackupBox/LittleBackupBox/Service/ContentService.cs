﻿using LittleBackupBox.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LittleBackupBox.Service
{
    public class ContentService : IContentService
    {
        public string GetContent(string contentKey, string language)
        {
            switch (contentKey)
            {
                case "LandingScreenViewModel_GetStarted":
                    return "Get Started";
                case "LandingScreenViewModel_TitleApp":
                    return "Little Backup Box";
                case "LandingScreenViewModel_Description":
                    return "A fully-automatic, pocketable photo backup.";
                case "LandingScreenViewModel_WarningText":
                    return "Please make sure that you're connected to the LBB-HOTSPOT";
                case "LandingScreenViewModel_ErrorText":
                    return "You are not connected to the LBB-HOTSPOT.";
                case "EraseDriveViewModel_ConfirmActionText":
                    return "Do you really want erase all data?";
                case "RebootDeviceViewModel_ConfirmActionText":
                    return "Do you really want reboot the device?";
                case "TurnOffDeviceViewModel_ConfirmActionText":
                    return "Do you really want turn off the device?";
                case "EraseDriveViewModel_ButtonYes":
                case "RebootDeviceViewModel_ButtonYes":
                case "TurnOffDeviceViewModel_ButtonYes":
                    return "Trust me, i know what i'm doing!";
                case "EraseDriveViewModel_ButtonNo":
                case "RebootDeviceViewModel_ButtonNo":
                case "TurnOffDeviceViewModel_ButtonNo":
                    return "Hum... Never mind...";
                case "EraseDriveViewModel_SucessMsg":
                    return "You successfully erase the drive";
                case "RebootDeviceViewModel_SucessMsg":
                    return "You successfully reboot the device";
                case "TurnOffDeviceViewModel_SucessMsg":
                    return "You successfully reboot the device";
                case "EraseDriveViewModel_SucessOk":
                case "EraseDriveViewModel_ErrorOk":
                case "RebootDeviceViewModel_SucessOk":
                case "RebootDeviceViewModel_ErrorOk":
                case "TurnOffDeviceViewModel_ErrorOk":
                case "TurnOffDeviceViewModel_SucessOk":
                    return "Ok";
                case "EraseDriveViewModel_ErrorTitle":
                case "RebootDeviceViewModel_ErrorTitle":
                case "TurnOffDeviceViewModel_ErrorTitle":
                    return "Error";
                case "EraseDriveViewModel_SucessTitle":
                case "RebootDeviceViewModel_SucessTitle":
                case "TurnOffDeviceViewModel_SucessTitle":
                    return "Alert";
                default:
                    return "";
            }
        }
    }
}
