﻿using LittleBackupBox.ViewModels;
using Xamarin.Forms;

namespace LittleBackupBox.Views
{
    public partial class TurnOffDevice : ContentPage
    {
        public TurnOffDevice()
        {
            InitializeComponent();
        }

        private void ButtonNo_Clicked(object sender, System.EventArgs e)
        {
            ((TurnOffDeviceViewModel)this.BindingContext).ButtonNo.Execute();
        }

        private void ButtonYes_Clicked(object sender, System.EventArgs e)
        {
            ((TurnOffDeviceViewModel)this.BindingContext).ButtonYes.Execute();
        }
    }
}
