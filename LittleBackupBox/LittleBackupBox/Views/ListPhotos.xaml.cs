﻿using LittleBackupBox.Model;
using LittleBackupBox.ViewModels;
using Xamarin.Forms;

namespace LittleBackupBox.Views
{
    public partial class ListPhotos : ContentPage
    {
        public ListPhotos()
        {
            InitializeComponent();
        }

        private async void OnItemTapped(object sender, ItemTappedEventArgs args)
        {
            ((ListPhotosViewModel)this.BindingContext).FileSelectedCommand.Execute((File)args.Item);
        }
    }
}
