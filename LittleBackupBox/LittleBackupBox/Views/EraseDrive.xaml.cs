﻿using LittleBackupBox.ViewModels;
using Xamarin.Forms;

namespace LittleBackupBox.Views
{
    public partial class EraseDrive : ContentPage
    {
        public EraseDrive()
        {
            InitializeComponent();
        }

        private void ButtonNo_Clicked(object sender, System.EventArgs e)
        {
            ((EraseDriveViewModel)this.BindingContext).ButtonNo.Execute();
        }

        private void ButtonYes_Clicked(object sender, System.EventArgs e)
        {
            ((EraseDriveViewModel)this.BindingContext).ButtonYes.Execute();
        }
    }
}
