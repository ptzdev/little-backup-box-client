﻿using LittleBackupBox.Interfaces;
using LittleBackupBox.ViewModels;
using Xamarin.Forms;

namespace LittleBackupBox.Views
{
    public partial class LandingScreen : ContentPage
    {
        public LandingScreen()
        {
            InitializeComponent();
        }

        private void ButtonGetStarted_Clicked(object sender, System.EventArgs e)
        {
            ((LandingScreenViewModel)this.BindingContext).GetStartedCommand.Execute();
        }
    }
}
