﻿using LittleBackupBox.ViewModels;
using Xamarin.Forms;

namespace LittleBackupBox.Views
{
    public partial class RebootDevice : ContentPage
    {
        public RebootDevice()
        {
            InitializeComponent();
        }

        private void ButtonNo_Clicked(object sender, System.EventArgs e)
        {
            ((RebootDeviceViewModel)this.BindingContext).ButtonNo.Execute();
        }

        private void ButtonYes_Clicked(object sender, System.EventArgs e)
        {
            ((RebootDeviceViewModel)this.BindingContext).ButtonYes.Execute();
        }
    }
}
