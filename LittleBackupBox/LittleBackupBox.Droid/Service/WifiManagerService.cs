﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using LittleBackupBox.Interfaces;
using Android.Net.Wifi;
using Xamarin.Forms;
using LittleBackupBox.Droid;

[assembly: Dependency(typeof(WifiManagerService))]
namespace LittleBackupBox.Droid
{
    public class WifiManagerService : IWifiManagerService
    {
        private bool isEmulator
        {
            get
            {
                string fing = Build.Fingerprint;
                bool isEmulator = false;
                if (fing != null)
                {
                    isEmulator = fing.Contains("vbox") || fing.Contains("generic");
                }
                return isEmulator;
            }
        }

        public bool IsExpectedSSID(string expectedSSID)
        {
            WifiManager wifiManager = (WifiManager)(Android.App.Application.Context.GetSystemService(Context.WifiService));

            if ((wifiManager != null && wifiManager.IsWifiEnabled) || isEmulator)
            {
                if (wifiManager.ConnectionInfo.SSID.ToLower() == expectedSSID.ToLower() || isEmulator)
                {
                    return true;
                }
            }

            return false;
        }
    }
}